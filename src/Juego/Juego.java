package Juego;


import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;
import Juego.Manzanas;
import Juego.Plantas;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	Laika perra;
	Manzanas[] manzanitas;
	Plantas[] plantas;
	double speed = 1;
	Image imgFondo = Herramientas.cargarImagen("fondoprov.png");
	// Variables y métodos propios de cada grupo
	// ...
	
	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Plantas Invasoras - Grupo 1", 800, 600);
		
		// Inicializar lo que haga falta para el juego
		// ...
		perra=new Laika(50,50);
		manzanitas = new Manzanas[6];
		plantas = new Plantas[2];
		
		//anguloFondo=0;
		
		for (int i=0; i < plantas.length;i++) {
			if(i%2 == 0) {

				plantas[i]=new Plantas(130*(i+1) ,50,1, 0 );
			}
			else {
				plantas[i]=new Plantas(130*(i) ,320,1, 0 );
			}
		}
		
		for (int i=0; i < manzanitas.length;i++) {
			if(i%2 == 0) {

				manzanitas[i]=new Manzanas(130*(i+1) ,200,0.25 );
			}
			else {
				manzanitas[i]=new Manzanas(130*(i) ,450,0.25 /*+ i/30.0*/);
			}
		}

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick()
	{
		// Procesamiento de un instante de tiempo
		// ...
		entorno.dibujarImagen(imgFondo, 400, 300, 0);
		
		if (entorno.estaPresionada(entorno.TECLA_DERECHA) && restriccionm(manzanitas,perra) != 1) {
			perra.mover(1, this.entorno);

		}
		if (entorno.estaPresionada(entorno.TECLA_ARRIBA) && restriccionm(manzanitas,perra) != 0) {
			perra.mover(0, this.entorno);

		}	

		if (entorno.estaPresionada(entorno.TECLA_ABAJO)&& restriccionm(manzanitas,perra) != 2) {
			perra.mover(2, this.entorno);

		}

		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)&& restriccionm(manzanitas,perra) != 3 ) {
			perra.mover(3,this.entorno);

		}

		for (int i=0; i < plantas.length;i++) {
			plantas[i].dibujarse(this.entorno);
		}
		
		for (int i=0; i < manzanitas.length;i++) {
			manzanitas[i].dibujarse(this.entorno);
		}

		perra.dibujarse(this.entorno);
		
		
	    for (int i = 0; i < plantas.length; i++ ) {
	    	plantas[i].moverAdelante(speed);
	    	plantas[i].detectarColisionBordes(entorno.ancho(), entorno.alto());
	    }
	    


		entorno.cambiarFont("Arial", 18, Color.white);

		entorno.escribirTexto("posicion en x:" + perra.x, 600, 50);
		entorno.escribirTexto("posicion en y:" + perra.y, 600, 100);



	}
	private int restriccionm(Manzanas[] m, Laika a) {
		for(int i=0; i < m.length;i++) {
			if(restriccion(m[i],a) < 5){
				 return restriccion(m[i],a);
			}
		}
		return 5;
	}

	public int restriccion(Manzanas m, Laika a) {
		double zona1=m.x-m.ancho/2;
		double zona2=m.y-m.alto/2;
		double zona0=m.y+m.alto/2;
		double zona3=m.x+m.ancho/2;
		if(a.y > zona2 && a.y < zona0 && a.x > zona1-20 && a.x < zona3) {
			return 1;
		}
		if(a.y > zona2 && a.y < zona0 && a.x > zona1 && a.x < zona3+20) {
			return 3;
		}
		if(a.y > zona2-20 && a.y < zona0 && a.x > zona1 && a.x < zona3) {
			return 2;
		}
		if(a.y > zona2 && a.y < zona0+20 && a.x > zona1 && a.x < zona3) {
			return 0;
		}
		return 5;
	}
	
	

	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}
