package Juego;

import java.awt.Color;

import java.awt.Image;
import java.awt.Rectangle;

import entorno.Entorno;
import entorno.Herramientas;

public class Laika {

	// Variables de instancia
	double x;
	double y;
	double ancho;
	double alto;
	Rectangle rlayka;
	int direccion;
	Image[] img;

	//constructor

	public Laika(double x, double y) {
		this.x = x;
		this.y = y;
		this.direccion = 1;
		this.img = new Image[4];
		for (int i=0; i < img.length ; i++) {

			img[i] = Herramientas.cargarImagen("corgicamina.png");

		}
		
		this.ancho=img[0].getWidth(null)*0.24;
		this.alto=img[0].getHeight(null)*0.24;
		rlayka=new Rectangle((int) this.x, (int)this.y, (int) this.ancho, (int) this.alto);
		//rlayka.
	}

	public void dibujarse(Entorno entorno)
	{
		entorno.dibujarImagen(img[this.direccion], this.x, this.y, 0, 0.15);
	}

	public void mover(int d, Entorno e)
	{

		this.direccion=d;

		if (direccion ==0)
		{    
			y--;	
		}
		if (direccion ==1)
		{
			x++;	
		}
		if (direccion ==2)
		{
			y++;	
		}
		if (direccion ==3)
		{
			x--;	
		}


		if(x > e.ancho()+50) {
			this.x=-50.0;
		}
		if (x < -50.0) {
			this.x=e.ancho()+50.0;	
		}
		if(y > e.alto()+50.0) {
			this.y=-50.0;
		}
		if (y < -50.0) {
			this.y=e.alto()+50.0;	
		}

}
}