package Juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Plantas {
	/* Variables de instancia*/
	double x;
	double y;
	double ancho;
	double alto;
	double direccion;
	Image img;
	double escala;
	
	public Plantas(double x, double y, double e, double d) {
		
		this.x = x;
		this.y = y;
		this.escala=e;
		this.direccion = d;
		img = Herramientas.cargarImagen("planticarni.png");
		this.ancho=img.getWidth(null)*this.escala;
		this.alto=img.getHeight(null)*this.escala;
		System.out.println("ancho "+this.ancho+"  alto "+this.alto);
	}
	public void moverAdelante(double speed) {
		this.x += Math.cos(this.direccion)*speed;
		this.y += Math.sin(this.direccion)*speed;
		/*
		if(this.x > 900) {
			this.x=-100;
		}
		if(this.x < -100) {
			this.x=900;
		}
		if(this.y > 650) {
			this.y=-50;
		}
		if(this.y < -50) {
			this.y=650;
		}*/
	
	}
	 public void detectarColisionBordes(int anchoPantalla, int altoPantalla) {
	        if (x + ancho / 2 >= anchoPantalla) {
	            // Colisión con el borde derecho, cambia la dirección hacia la izquierda
	            direccion = Math.PI - direccion;
	        } else if (x - ancho / 2 <= 0) {
	            // Colisión con el borde izquierdo, cambia la dirección hacia la derecha
	            direccion = Math.PI - direccion;
	        }
	        if (y + alto / 2 >= altoPantalla) {
	            // Colisión con el borde inferior, cambia la dirección hacia arriba
	            direccion = -direccion;
	        } else if (y - alto / 2 <= 0) {
	            // Colisión con el borde superior, cambia la dirección hacia abajo
	            direccion = -direccion;
	        }
	    }

	
	public void dibujarse(Entorno entorno)
	{
		entorno.dibujarImagen(img, this.x, this.y, 0, this.escala);
	}
	
}
